# tic-tac-toe

A Tic Tac Toe command line app, written in Crystal.

## Installation

### Pre-requisites:

Requires Crystal. To install on OS X (see [docs](https://crystal-lang.org/docs/installation/) for full details):

```bash
brew update
brew install crystal-lang
```

### Running the application

To compile and run the program: `crystal ./src/tic-tac-toe.cr`

To create an executable: `crystal build ./src/tic-tac-toe.cr`. Alternatively, to create a fully optimised executable: `crystal build ./src/tic-tac-toe.cr --release`

The build will generate an executable, which can be run with `./tic-tac-toe`.

### Running the tests

To run the main test suite: `crystal spec ./spec/`

To run a performance test, which compares the four game algorithms, with a 3x3 board with 7 moves remaining: `crystal spec ./performance/`

## About

### Computer Player

The Computer Player can run 4 different variations of MiniMax:

- `MiniMax`: standard Mini Max implementation with optional maximum depth
- `AlphaBeta`: standard implementation of Alpha Beta pruning with Mini Max
- `ConcurrentMiniMax`: runs each game move concurrently (including in recursive calls)
- `OneDeepConcurrentMiniMax`: runs each initial game move concurrently

Running the performance test, should lead to results in line with the following:

```bash
Times in milliseconds:
MiniMax: 169
ConcurrentMiniMax: 391
OneDeepConcurrentMiniMax: 143
AlphaBeta: 11
Speed difference AlphaBeta to MiniMax: 14 times
```

From these results, we can see `MiniMaxConcurrent` takes around 2.3 times as long as `MiniMax`, whereas `OneDeepConcurrentMiniMax` takes about 0.8 times as long.

Currently Crystal's concurrency model doesn't support parallelism, instead it supports Fibers, which are described in the [docs](https://crystal-lang.org/docs/guides/concurrency.html) as:

> A fiber is in a way similar to an operating system thread except that it's much more lightweight and its execution is managed internally by the process. So, a program will spawn multiple fibers and Crystal will make sure to execute them when the time is right.

In this light, the performance of ConcurrentMiniMax, which is spawning up to 5040 (7 factorial less any games finishing before the board is full), gives an indication of the cost of a Fiber.

Both concurrent algorithms use Crystal's implementation of channels.

When playing the game, if a computer player is selected, there is an option to select one of the four algorithms available. On first move, the speed improvement of `AlphaBeta` is quite clear.

### Type Aliases
Crystal supports type aliases, note that these are simply references to underlying types. This is why, for example, the `Game::Game` implementation of `AI::Playable` compiles even though the `Game::Game` type aliases differ from those used by `AI::Playable`, as Crystal is checking the underlying types, not the type alias names.

### Macros
Crystal supports Macros, which are demonstrated in the `TicTacToe::App::ColourFormatter` class.

### Test Doubles
Crystal's built in test framework does not provide a test double framework. Whilst there is a 3rd-party Shard for test doubles, it is not clear whether this is the de-facto choice for test doubles in Crystal (shards are downloaded directly from Github, so it is hard to see how popular any shard actually is). I have therefore implemented my own custom test doubles.

### 4x4 Support
I have not added 4x4 support, as this leads to more code but adds little to the design of the code. Should this feature be needed, it would primarily require some updates to the `BoardFormatter` and `BoardStatus` classes, and an update to the maximum depth search in the AI algorithms.

### Union Types and Null values
Crystal supports Union Types, and therefore Option types (which is just the union of a type with null), which means a return type which does not expressly allow for null values, must return the specified type.

An example of the impact of enforcing non-null types can be seen in the need for a default return value in `TicTacToe::AI::AlphaBeta::select_result`, which is set as a special `dead_path_result` 

### Named arguments
All arguments can be specified by name as well as position (see [doc](https://crystal-lang.org/docs/syntax_and_semantics/default_and_named_arguments.html)). This is helpful when providing `max_depth` to the different AI algorithms.
