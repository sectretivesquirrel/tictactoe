require "../spec/spec_helper"
require "../src/ai/ai"
require "../src/ai/mini_max"
require "../src/ai/alpha_beta"
require "../src/ai/concurrent_mini_max"
require "../src/ai/one_deep_concurrent_mini_max"

module TicTacToe
	module AI
		describe "AI" do
			context "speed comparison:" do
				# x 2 3
				# o 5 6
				# 7 8 9
				game = TicTacToe.create_game_with_moves([1, 4])

				it "alpha_beta is faster than mini_max for multiple moves" do
					mm_score, mm_time = run_with_timings(MiniMax.new, game)
					ab_score, ab_time = run_with_timings(AlphaBeta.new, game)
					ccmm_score, ccmm_time = run_with_timings(ConcurrentMiniMax.new, game)
					odccmm_score, odccmm_time = run_with_timings(OneDeepConcurrentMiniMax.new, game)
					speed_difference = mm_time.total_nanoseconds / ab_time.total_nanoseconds
					mm_score.should eq(2)
					ab_score.should eq(2)
					ccmm_score.should eq(2)
					puts("Times in milliseconds:")
					puts("MiniMax: #{mm_time.milliseconds}")
					puts("ConcurrentMiniMax: #{ccmm_time.milliseconds}")
					puts("OneDeepConcurrentMiniMax: #{odccmm_time.milliseconds}")
					puts("AlphaBeta: #{ab_time.milliseconds}")
					puts("Speed difference AlphaBeta to MiniMax: #{speed_difference.to_i} times")
					(speed_difference > 6).should be_true
				end
			end
		end

		def self.run_with_timings(ai : AI, game : Game::Game) : Tuple(Game::Square, Time::Span)
			start_time = Time.now
			move = ai.select_move(game).first
			end_time = Time.now
			{move, end_time - start_time}
		end
	end
end
