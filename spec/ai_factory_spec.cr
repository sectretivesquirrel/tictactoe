require "./spec_helper"
require "../ai/ai_factory"

module TicTacToe
	module AI
		describe AIFactory do
			ai_factory = AIFactory.new

			context "with AIType::MiniMax" do
				it "returns a MiniMax" do
					ai_factory.build(AIType::MiniMax).should be_a(MiniMax)
				end

				it "does not return a ConcurrentMiniMax" do
					ai_factory.build(AIType::MiniMax).should_not be_a(ConcurrentMiniMax)
				end

				it "does not return an AlphaBeta" do
					ai_factory.build(AIType::MiniMax).should_not be_a(AlphaBeta)
				end
			end

			context "with AIType::ConcurrentMiniMax" do
				it "returns a ConcurrentMiniMax" do
					ai_factory.build(AIType::ConcurrentMiniMax).should be_a(ConcurrentMiniMax)
				end
			end

			context "with AIType::OneDeepConcurrentMiniMax" do
				it "returns a OneDeepConcurrentMiniMax" do
					ai_factory.build(AIType::OneDeepConcurrentMiniMax).should be_a(OneDeepConcurrentMiniMax)
				end
			end

			context "with AIType::AlphaBeta" do
				it "returns a AlphaBeta" do
					ai_factory.build(AIType::AlphaBeta).should be_a(AlphaBeta)
				end
			end
		end
	end
end
