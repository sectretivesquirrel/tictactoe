require "./spec_helper"
require "../src/app/ai_type_selector"
require "./double/cli_double"

module TicTacToe
	module App
		describe AITypeSelector do
			cli = CLIDouble.new
			ai_type_selector = AITypeSelector.new(cli)
			selected = ai_type_selector.get_selection

			it "asks the view to clear the screen" do
				cli.clear_screen_called.should be_true
			end

			it "writes formatted AI Types to CLI" do
				cli.write_received.should eq("<1> AlphaBeta (* default)\n<2> MiniMax\n<3> ConcurrentMiniMax\n<4> OneDeepConcurrentMiniMax")
			end

			context "user selects option 1" do
				cli.read_responds_with = "1"

				it "responds with AlphaBeta" do
					ai_type_selector.get_selection.should eq(AI::AIType::AlphaBeta)
				end
			end

			context "user selects option 2" do
				cli.read_responds_with = "2"

				it "responds with MiniMax" do
					ai_type_selector.get_selection.should eq(AI::AIType::MiniMax)
				end
			end

			context "user selects option 3" do
				cli.read_responds_with = "3"

				it "responds with ConcurrentMiniMax" do
					ai_type_selector.get_selection.should eq(AI::AIType::ConcurrentMiniMax)
				end
			end

			context "user selects option 4" do
				cli.read_responds_with = "4"

				it "responds with OneDeepConcurrentMiniMax" do
					ai_type_selector.get_selection.should eq(AI::AIType::OneDeepConcurrentMiniMax)
				end
			end

			context "user selects any other number" do
				cli.read_responds_with = "5"

				it "responds with AlphaBeta" do
					ai_type_selector.get_selection.should eq(AI::AIType::AlphaBeta)
				end
			end

			context "user enters anything else" do
				cli.read_responds_with = "gibberish"

				it "responds with AlphaBeta" do
					ai_type_selector.get_selection.should eq(AI::AIType::AlphaBeta)
				end
			end
		end
	end
end
