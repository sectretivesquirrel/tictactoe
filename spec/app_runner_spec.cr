require "./spec_helper"
require "../src/app/app_runner"
require "../src/app/game_selector"
require "./double/game_view_double"
require "./double/game_selector_double"
require "./double/game_factory_double"
require "./double/play_again_selector_double"
require "./double/ai_type_selector_double"

module TicTacToe
	module App
		describe AppRunner do
			game = Game::GameDouble.new
			game.over = true
			game_factory = Game::GameFactoryDouble.new(game)
			game_view = GameViewDouble.new
			ai_type_selector = AITypeSelectorDouble.new

			context "don't play again" do
				game_selector = GameSelectorDouble.new
				game_type = {Player::PlayerType::Human, Player::PlayerType::Human}
				game_selector.get_selection_responds_with = game_type
				play_again_selector = PlayAgainSelectorDouble.new
				play_again_selector.play_again_responds_with << false
				app_runner = AppRunner.new(game_selector, game_factory, game_view, play_again_selector, ai_type_selector)
				app_runner.run

				it "asks the game selector for the game type" do
					game_selector.get_selection_called.should be_true
				end

				it "doesn't ask the ai type selector for an ai type" do
					ai_type_selector.get_selection_called.should be_false
				end

				it "asks the game factory to build a human vs human game (no AI needed)" do
					game_factory.build_receieved.should eq({game_type, nil})
				end

				it "creates a game runner and runs the game" do
					game.method_calls.includes?(:over?).should be_true
				end

				it "asks the play again selector whether to play again" do
					play_again_selector.play_again_called.should be_true
				end
			end

			context "play again" do
				game_selector = GameSelectorDouble.new
				game_type = {Player::PlayerType::Computer, Player::PlayerType::Human}
				game_selector.get_selection_responds_with = game_type
				play_again_selector = PlayAgainSelectorDouble.new
				play_again_selector.play_again_responds_with << true << false
				selected_ai_type = AI::AIType::AlphaBeta
				ai_type_selector.get_selection_responds_with = selected_ai_type

				app_runner = AppRunner.new(game_selector, game_factory, game_view, play_again_selector, ai_type_selector)
				app_runner.run

				it "asks to play again twice" do
					play_again_selector.play_again_call_count.should eq(2)
				end

				it "asks the ai type selector for an ai type" do
					ai_type_selector.get_selection_called.should be_true
				end

				it "asks the game factory to build a human vs human game (no AI needed)" do
					game_factory.build_receieved.should eq({game_type, selected_ai_type})
				end
			end
		end
	end
end
