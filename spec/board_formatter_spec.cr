require "./spec_helper"
require "../src/app/board_formatter"

module TicTacToe
	module App
		describe BoardFormatter do
			board_formatter = BoardFormatter.new

			context "empty board" do
				it "formats the empty board" do
					board_formatter.format(Game::Board.new).should eq(TicTacToe.formatted_board_string([1, 2, 3, 4, 5, 6, 7, 8, 9]))
				end
			end

			context "incomplete board" do
				it "formats the incomplete board with moves correctly assigned to X and O" do
					board = Game::Board.new
					f = ColourFormatter.new
					TicTacToe.make_moves(board, [1, 5, 9])
					board_formatter.format(board).should eq(TicTacToe.formatted_board_string([f.yellow(X), 2, 3, 4, f.magenta(O), 6, 7, 8, f.red(X)]))
				end
			end
		end
	end
end
