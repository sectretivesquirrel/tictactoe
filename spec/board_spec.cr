require "./spec_helper"
require "../src/game/board"
require "../src/game/board_status"

module TicTacToe
	module Game
		describe Board do
			context "a cloned board" do
				cloaning_board = Board.new
				cloaning_board.move!(2)
				frozen_squares = cloaning_board.squares.dup
				frozen_moves = cloaning_board.moves.dup
				cloaned_board = cloaning_board.clone
				cloaning_board.move!(1)

				it "has its own squares" do
					cloaned_board.squares.should_not be(cloaning_board.squares)
				end

				it "has the same squares as the cloing board had at the time it was cloned" do
					cloaned_board.squares.should eq(frozen_squares)
				end

				it "has its own moves" do
					cloaned_board.moves.should_not be(cloaning_board.moves)
				end

				it "has the same moves as the cloing board had at the time it was cloned" do
					cloaned_board.moves.should eq(frozen_moves)
				end

				it "does not change the cloaning board" do
					cloaning_board.moves.should eq([2, 1])
				end
			end

			context "new board" do
				board = Board.new

				it "has no last move" do
					board.last_move.should be_nil
				end

				it "reports current player is X" do
					board.current_player_symbol.should eq(X)
				end

				it "is not full" do
					BoardStatus.new(board).full?.should be_false
				end

				it "does not have a winner" do
					BoardStatus.new(board).winner?.should be_false
				end

				it "reports all squares are available" do
					board.available_squares.should eq([1, 2, 3, 4, 5, 6, 7, 8, 9])
				end
			end

			context "board after first move" do
				first_move = 1
				board = Board.new
				board.move!(first_move)

				it "reports the last move is the first move" do
					board.last_move.should eq(first_move)
				end

				it "reports the current player is O" do
					board.current_player_symbol.should eq(O)
				end

				it "is not full" do
					BoardStatus.new(board).full?.should be_false
				end

				it "does not have a winner" do
					BoardStatus.new(board).winner?.should be_false
				end

				it "reports all squares are available except the first move square" do
					board.available_squares.should eq([2, 3, 4, 5, 6, 7, 8, 9])
				end
			end

			context "board after second move" do
				first_move = 1
				second_move = 5
				board = Board.new
				board.move!(first_move)
				board.move!(second_move)

				it "reports the last move is the second move" do
					board.last_move.should eq(second_move)
				end

				it "reports the current player is X" do
					board.current_player_symbol.should eq(X)
				end

				it "is not full" do
					BoardStatus.new(board).full?.should be_false
				end

				it "reports all squares are available except the two move squares" do
					board.available_squares.should eq([2, 3, 4, 6, 7, 8, 9])
				end
			end

			context "board after all moves" do
				board = Board.new
				[1, 2, 3, 4, 5, 7, 6, 9, 8].each { |square| board.move!(square) }

				it "is drawn" do
					BoardStatus.new(board).drawn?.should be_true
				end

				it "is full" do
					BoardStatus.new(board).full?.should be_true
				end

				it "reports no squares are available" do
					board.available_squares.should eq([] of Int32)
				end
			end

			context "attempting an invalid move" do
				board = Board.new
				board.move!(5)

				it "raises an illegal move error" do
					expect_raises(BoardException, BoardException.illegal_move.message) do
						board.move!(5)
					end
				end
			end
		end
	end
end
