require "./spec_helper"
require "../src/game/board_status"
require "../src/game/player_symbol"
require "./double/board_double"

module TicTacToe
	module Game
		describe BoardStatus do
			context "empty board" do
				board = BoardDouble.new
				board_status = BoardStatus.new(board)

				it "is not complete" do
					board_status.complete?.should be_false
				end

				it "doesn't have a winner" do
					board_status.winner?.should be_false
					board_status.winner.should be_nil
				end
			end

			context "drawn board" do
				board = BoardDouble.new(DRAWN_BOARD_SQUARES)
				board_status = BoardStatus.new(board)

				it "is complete" do
					board_status.complete?.should be_true
				end

				it "is drawn" do
					board_status.drawn?.should be_true
				end
			end

			context "winning row for X" do
				board = BoardDouble.new(WINNING_ROW_SQUARES)
				run_winning_tests(BoardStatus.new(board), X)
			end

			context "winning column for O" do
				board = BoardDouble.new(WINNING_COLUMN_SQUARES)
				run_winning_tests(BoardStatus.new(board), O)
			end

			context "winning diagonal" do
				board = BoardDouble.new(WINNING_DIAGANOL_SQUARES)
				run_winning_tests(BoardStatus.new(board), O)
			end
		end

		def self.run_winning_tests(board_status : BoardStatus, winner : PlayerSymbol)
			it "is complete" do
				board_status.complete?.should be_true
			end

			it "has a winner" do
				board_status.winner?.should be_true
			end

			it "reports winner is #{winner}" do
				board_status.winner.should eq(winner)
			end
		end
	end
end
