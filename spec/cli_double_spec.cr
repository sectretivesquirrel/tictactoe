require "./spec_helper"
require "../src/app/cli"
require "./double/cli_double"

module TicTacToe
	module App
		describe CLIDouble do
			it "is a CLI" do
				CLIDouble.new.should be_a(CLI)
			end

			context "write is called with some input" do
				it "written responds with the input" do
					input = "This is a test"
					cli_double = CLIDouble.new
					cli_double.write(input)
					cli_double.write_received.should eq(input)
				end
			end

			context "write has not been called" do
				it "written responds with nil" do
					cli_double = CLIDouble.new
					cli_double.write_received.should be_nil
				end
			end

			context "to_read has been called with output" do
				it "read responds with the output" do
					output = "This is a test"
					cli_double = CLIDouble.new
					cli_double.read_responds_with = output
					cli_double.read.should eq(output)
				end
			end

			context "to_read has not been called" do
				it "read responds with nil" do
					cli_double = CLIDouble.new
					cli_double.read.should be_nil
				end
			end
		end
	end
end
