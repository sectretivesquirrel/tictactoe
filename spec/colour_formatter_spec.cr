require "./spec_helper"
require "../src/app/colour_formatter"

module TicTacToe
	module App
		describe ColourFormatter do
			test_output = "This is some test output"
			blue = "\u001B[34m"
			reset = "\u001B[0m"
			yellow = "\u001B[33m"
			green = "\u001B[32m"
			red = "\u001B[31m"
			magenta = "\u001B[35m"

			colour_formatter = ColourFormatter.new

			it "can format output as blue" do
				colour_formatter.blue(test_output).should eq(blue + test_output + reset)
			end

			it "can format output as yellow" do
				colour_formatter.yellow(test_output).should eq(yellow + test_output + reset)
			end

			it "can format output as green" do
				colour_formatter.green(test_output).should eq(green + test_output + reset)
			end

			it "can format output as red" do
				colour_formatter.red(test_output).should eq(red + test_output + reset)
			end

			it "can format output as magenta" do
				colour_formatter.magenta(test_output).should eq(magenta + test_output + reset)
			end
		end
	end
end
