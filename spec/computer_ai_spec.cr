require "./spec_helper"
require "../src/ai/ai"
require "../src/ai/mini_max"
require "../src/ai/alpha_beta"
require "../src/ai/concurrent_mini_max"

module TicTacToe
	module AI
		describe MiniMax do
			context "one square remaining:" do
				# x o x
				# x o o
				# 7 x o
				game = TicTacToe.create_game_with_moves([1, 2, 3, 5, 4, 6, 8, 9])

				it "selects the winning move, with a maximum winning score" do
					TicTacToe.run_ai_tests(game, 7)
				end
			end

			context "two squares remaining, one winning move:" do
				# x o x
				# x o o
				# 7 8 x
				game = TicTacToe.create_game_with_moves([1, 2, 3, 5, 4, 6, 9])

				it "selects the winning move with a maximum winning score" do
					TicTacToe.run_ai_tests(game, 8)
				end
			end

			context "two squares remaining, one drawing move, one losing move:" do
				# o x o
				# x x o
				# 7 8 x
				game = TicTacToe.create_game_with_moves([2, 1, 4, 3, 5, 6, 9])

				it "selects the drawing move" do
					TicTacToe.run_ai_tests(game, 8)
				end
			end

			context "losing position:" do
				# See http://neverstopbuilding.com/minimax (Smart/Dumb example for visualisation)
				# 1 x 3
				# 4 5 x
				# o o x
				game = TicTacToe.create_game_with_moves([2, 7, 6, 8, 9])

				it "selects the move which delays losing for as long as possible" do
					TicTacToe.run_ai_tests(game, 3)
				end
			end

			context "limiting the depth search:" do
				# 1 x 3
				# 4 5 x
				# o o x
				game = TicTacToe.create_game_with_moves([2, 7, 6, 8, 9])

				it "can affect the result" do
					TicTacToe.run_ai_tests(game, 3, max_depth: 2)
					TicTacToe.run_ai_tests(game, 1, max_depth: 1)
				end
			end
		end
	end
end
