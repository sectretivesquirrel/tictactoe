require "./spec_helper"
require "../player/computer_player"
require "./double/ai_factory_double"

module TicTacToe
	module Player
		describe ComputerPlayer do
			context "default AIType" do
				it "request an AlphaBeta AI from the AIFactory" do
					ai_factory = AI::AIFactoryDouble.new
					computer_player = ComputerPlayer.new(ai_factory, sleep_time: 0)
					computer_player.select_move(TicTacToe.create_game)
					ai_factory.build_received.should eq(AI::AIType::AlphaBeta)
				end
			end

			context "AIType of MiniMax" do
				it "request a MiniMax AI from the AIFactory" do
					ai_factory = AI::AIFactoryDouble.new
					computer_player = ComputerPlayer.new(ai_factory, AI::AIType::MiniMax, sleep_time: 0)
					computer_player.select_move(TicTacToe.create_game)
					ai_factory.build_received.should eq(AI::AIType::MiniMax)
				end
			end

			context "AIType of ConcurrentMiniMax" do
				it "request a MiniMax AI from the AIFactory" do
					ai_factory = AI::AIFactoryDouble.new
					computer_player = ComputerPlayer.new(ai_factory, AI::AIType::ConcurrentMiniMax, sleep_time: 0)
					computer_player.select_move(TicTacToe.create_game)
					ai_factory.build_received.should eq(AI::AIType::ConcurrentMiniMax)
				end
			end

			context "AIType of OneDeepConcurrentMiniMax" do
				it "request a MiniMax AI from the AIFactory" do
					ai_factory = AI::AIFactoryDouble.new
					computer_player = ComputerPlayer.new(ai_factory, AI::AIType::OneDeepConcurrentMiniMax, sleep_time: 0)
					computer_player.select_move(TicTacToe.create_game)
					ai_factory.build_received.should eq(AI::AIType::OneDeepConcurrentMiniMax)
				end
			end
		end
	end
end
