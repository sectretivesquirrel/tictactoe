require "../../src/ai/ai_factory"
require "../../src/ai/alpha_beta"
require "./ai_double"

module TicTacToe
	module AI
		class AIFactoryDouble < AIFactory
			getter :build_received
			@build_received : AIType?

			def initialize
				@build_called = false
			end

			def build(ai_type : AIType, max_depth : Int32) : AI
				@build_called = true
				@build_received = ai_type
				AIDouble.new
			end
		end
	end
end
