require "../../src/app/ai_type_selector"
require "./cli_double"

module TicTacToe
	module App
		class AITypeSelectorDouble < AITypeSelector
			getter :get_selection_called
			setter :get_selection_responds_with


			def initialize
				super(CLIDouble.new)
				@get_selection_called = false
				@get_selection_responds_with = AI::AIType::MiniMax
			end

			def get_selection : AI::AIType
				@get_selection_called = true
				@get_selection_responds_with
			end
		end
	end
end
