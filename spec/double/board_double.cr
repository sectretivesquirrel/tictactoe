require "../../src/game/board"

module TicTacToe
	module Game
		class BoardDouble < Board
			setter :full

			def initialize(squares : Line? = nil)
				super()
				@squares = squares unless squares.nil?
				@full = false
			end

			def full?
				@full
			end
		end
	end
end
