require "../../../src/app/cli"

module TicTacToe
	module App
		class CLIDouble < CLI
			getter :write_received, clear_screen_called
			setter :read_responds_with

			@read_responds_with : String?

			def initialize
				@clear_screen_called = false
			end

			def read
				@read_responds_with
			end

			def write(@write_received : String)
			end

			def clear_screen
				@clear_screen_called = true
			end
		end
	end
end
