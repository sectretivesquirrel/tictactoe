require "../../src/player/computer_player"
require "../../src/ai/ai_type"

module TicTacToe
	module Player
		class ComputerPlayerDouble < ComputerPlayer
			getter :select_move_called, :select_move_reponds_with

			def initialize(ai_factory : AI::AIFactory, ai_type : AI::AIType? = nil)
				super
				@select_move_called = false
				@select_move_reponds_with = 1
			end

			def select_move(game : Game::Game) : Int32
				@select_move_called = true
				@select_move_reponds_with
			end
		end
	end
end
