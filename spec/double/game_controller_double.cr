require "../../src/app/game_controller"
require "./game_double"
require "./game_view_double"

module TicTacToe
	module App
		class GameControllerDouble < GameController
			getter :game, :game_view, :next_call_count
			property :moves_remaining

			def initialize(game = Game::GameDouble.new, game_view = GameViewDouble.new)
				super
				@moves_remaining = 0
				@next_call_count = 0
			end

			def next?
				@next_call_count += 1
				@moves_remaining -= 1
				@moves_remaining > 0
			end
		end
	end
end
