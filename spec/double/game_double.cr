require "../../src/game/game"
require "../../src/game/board"
require "./player_double"

module TicTacToe
	module Game
		class GameDouble < Game
			property :current_player_symbol, :current_player
			setter :over 
			getter :board, :move_received, :method_calls

			@move_receieved : Int32?
			@current_player : Player::Player

			def initialize(@board = Board.new)
				super(@board, Player::PlayerDouble.new, Player::PlayerDouble.new)
				@over = false
				@current_player_symbol = PlayerSymbol::X
				@current_player = Player::PlayerDouble.new
				@method_calls = [] of Symbol
			end

			def over?
				called(:over?)
				@over
			end

			def move!(square : Square)
				called(:move)
				@move_received = square
			end

			private def called(method : Symbol)
				@method_calls << method
			end
		end
	end
end
