require "../../src/game/game_factory"
require "../../src/app/game_selector"
require "../../src/ai/ai_type"
require "./game_double"

module TicTacToe
	module Game
		class GameFactoryDouble < GameFactory
			getter :build_receieved

			@build_receieved : {GameType, AI::AIType?}?

			def initialize(@game = GameDouble.new)
				super()
			end

			def build(game_type : GameType, ai : AI::AIType?)
				@build_receieved = {game_type, ai}
				@game
			end
		end
	end
end
