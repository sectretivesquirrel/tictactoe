require "../../src/app/game_selector"
require "./game_selector_view_double"

module TicTacToe
	module App
		class GameSelectorDouble < GameSelector
			getter :get_selection_called
			setter :get_selection_responds_with

			def initialize(game_selector_view = GameSelectorViewDouble.new)
				super
				@get_selection_called = false
				@get_selection_responds_with = {Player::PlayerType::Human, Player::PlayerType::Human}
			end

			def get_selection
				@get_selection_called = true
				@get_selection_responds_with
			end
		end
	end
end
