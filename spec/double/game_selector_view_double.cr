require "../../src/app/game_selector_view"
require "./cli_double"

module TicTacToe
	module App
		class GameSelectorViewDouble < GameSelectorView
			getter :game_options_received, :select, :report_invalid_input_called, :clear_screen_called

			def initialize(@cli = CLIDouble.new)
				super
				@select = [] of String?
				@report_invalid_input_called = false
				@clear_screen_called = false
			end

			def list_game_options(@game_options_received : Array(GameType))
			end

			def request_selection
				@select.pop
			end

			def report_invalid_input
				@report_invalid_input_called = true
			end

			def clear_screen
				@clear_screen_called = true
			end
		end
	end
end
