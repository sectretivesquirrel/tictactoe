require "../../src/app/game_view"
require "../../src/game/board"
require "../../src/game/game"
require "../../src/game/player_symbol"
require "./cli_double"

module TicTacToe
	module App
		class GameViewDouble < GameView
			getter :next_move, :winner_reported, :display_board_receieved, :request_move_received, :method_calls,
				:warn_invalid_input_called

			@display_board_receieved : Game::Board?
			@request_move_received : Game::PlayerSymbol?

			def initialize
				super(CLIDouble.new)
				@winner_reported = false
				@warn_invalid_input_called = false
				@next_move = [] of String
				@method_calls = [] of Symbol
			end

			def clear_screen
				called(:clear_screen)
			end

			def display_board(board : Game::Board)
				called(:display_board)
				@display_board_receieved = board
			end

			def request_move(player_symbol : Game::PlayerSymbol) : String
				called(:request_move)
				@request_move_received = player_symbol
				@next_move.pop || "1"
			end

			def report_game_status(game : Game::Game)
				called(:report_game_status)
				@winner_reported = true
			end

			def warn_invalid_input
				@warn_invalid_input_called = true
			end

			private def called(method : Symbol)
				@method_calls << method
			end
		end
	end
end
