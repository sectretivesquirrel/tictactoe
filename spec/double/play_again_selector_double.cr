require "../../src/app/play_again_selector"
require "./cli_double"

module TicTacToe
	module App
		class PlayAgainSelectorDouble < PlayAgainSelector
			getter :play_again_called, :play_again_responds_with, :play_again_call_count

			def initialize
				super(CLIDouble.new)
				@play_again_called = false
				@play_again_responds_with = [] of Bool
				@play_again_call_count = 0
			end

			def play_again?
				@play_again_called = true
				@play_again_call_count += 1
				@play_again_responds_with.shift
			end
		end
	end
end
