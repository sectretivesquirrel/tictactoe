require "../../src/player/player"

module TicTacToe
	module Player
		class PlayerDouble < Player
			def initialize
				@is_human = true
			end

			def human?
				@is_human
			end
		end
	end
end
