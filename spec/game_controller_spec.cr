require "./spec_helper"
require "../src/app/game_controller"
require "./double/game_double"
require "./double/game_view_double"
require "./double/computer_player_double"
require "./double/ai_factory_double"

module TicTacToe
	module App
		describe GameController do
			context "game is over:" do
				game = Game::GameDouble.new
				game.over = true
				game_view = GameViewDouble.new
				controller = GameController.new(game, game_view)
				next? = controller.next?

				it "checks if the game is over" do
					game.method_calls[0].should eq(:over?)
				end

				it "clears the screen" do
					game_view.method_calls[0].should eq(:clear_screen)
				end

				it "asks the view to display the board" do
					game_view.method_calls[1].should eq(:display_board)
					game_view.display_board_receieved.should be(game.board)
				end

				it "asks the view to report the game status" do
					game_view.method_calls[2].should eq(:report_game_status)
					game_view.winner_reported.should be_true
				end

				it "responds that there are no more game states to report" do
					next?.should be_false
				end
			end

			context "active game, human player's move:" do
				player_move = 3
				game = Game::GameDouble.new
				game_view = GameViewDouble.new
				game_view.next_move << player_move.to_s
				controller = GameController.new(game, game_view)
				next? = controller.next?

				it "checks if the game is over" do
					game.method_calls[0].should eq(:over?)
				end

				it "clears the screen" do
					game_view.method_calls[0].should eq(:clear_screen)
				end

				it "asks the view to display the game board" do
					game_view.method_calls[1].should eq(:display_board)
					game_view.display_board_receieved.should be(game.board)
				end

				it "requests the current player's move" do
					game_view.method_calls[2].should eq(:request_move)
					game_view.request_move_received.should eq(game.current_player_symbol)
				end

				it "tells the game to make the player's move" do
					game.method_calls.uniq[1].should eq(:move)
					game.move_received.should eq(player_move)
				end

				it "responds that there are more game states to report" do
					next?.should be_true
				end
			end

			context "active game, human player's move is initialy invalid:" do
				game = Game::GameDouble.new
				game_view = GameViewDouble.new
				game_view.next_move << 1.to_s << "invalid move"
				controller = GameController.new(game, game_view)
				controller.next?

				it "asks the view to warn the player's input is invalid" do
					game_view.warn_invalid_input_called.should be_true
				end
			end

			context "active game, computer player's move:" do
				computer_player = Player::ComputerPlayerDouble.new(AI::AIFactoryDouble.new)
				game = Game::GameDouble.new
				game.current_player = computer_player
				game_view = GameViewDouble.new
				controller = GameController.new(game, game_view)
				controller.next?

				it "requests the computer player's move" do
					computer_player.select_move_called.should be_true
				end
			end
		end
	end
end
