require "./spec_helper"
require "../src/game/game_factory"
require "../src/game/player_symbol"
require "../src/game/game"
require "../src/player/player_type"
require "../src/player/human_player"
require "../src/player/computer_player"
require "../src/ai/ai_type"


module TicTacToe
	module Game
		describe GameFactory do
			context "with a Human vs Human GameType" do
				game_type = {Player::PlayerType::Human, Player::PlayerType::Computer}
				game = GameFactory.new.build(game_type, AI::AIType::MiniMax)

				it "builds a Game instance" do
					game.should be_a(Game)
				end

				it "builds a Game with a Human Player X" do
					game.current_player_symbol.should eq(X)
					game.current_player.should be_a(Player::HumanPlayer)
				end

				it "builds a Game with a Computer Player O" do
					game.move!(1)
					game.current_player_symbol.should eq(O)
					game.current_player.should be_a(Player::ComputerPlayer)
				end
			end
		end
	end
end
