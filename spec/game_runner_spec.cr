require "./spec_helper"
require "../src/app/game_runner"
require "./double/game_controller_double"

module TicTacToe
	module App
		describe GameRunner do
			context "game with one remaining move" do
				test_game_with_moves_remaining(1)
			end

			context "game with nine remaining moves" do
				test_game_with_moves_remaining(9)
			end
		end

		def self.test_game_with_moves_remaining(moves_remaining : Int32)
			it "runs a game #{moves_remaining} times" do
				controller = GameControllerDouble.new
				controller.moves_remaining = moves_remaining
				runner = GameRunner.new(controller)
				runner.run
				controller.next_call_count.should eq(moves_remaining)
			end
		end
	end
end
