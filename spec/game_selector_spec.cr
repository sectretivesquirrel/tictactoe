require "./spec_helper"
require "../src/app/game_selector"
require "./double/game_selector_view_double"

module TicTacToe
	module App
		describe GameSelector do
			context "user will select option 1 first time:" do
				view = GameSelectorViewDouble.new
				view.select  << "1"
				game_selector = GameSelector.new(view)
				selected = game_selector.get_selection

				it "asks the view to clear the screen" do
					view.clear_screen_called.should be_true
				end

				it "writes options to output" do
					view.game_options_received.should eq(GameSelector.game_options)
				end

				it "responds with the user's selection" do
					selected.should eq(GameSelector.game_options[0])
				end
			end

			context "user will eventually select option 2:" do
				view = GameSelectorViewDouble.new
				view.select  << "2" << "0" << "5" << "invalid"
				game_selector = GameSelector.new(view)
				selected = game_selector.get_selection

				it "warns of invalid user input" do
					view.report_invalid_input_called.should eq(true)
				end

				it "responds with the user's valid selection" do
					selected.should eq(GameSelector.game_options[1])
				end
			end
		end
	end
end
