require "./spec_helper"
require "../src/app/game_selector_view"
require "../src/app/game_selector"
require "../src/game/game_factory"
require "./double/cli_double"

module TicTacToe
	module App
		describe GameSelectorView do
			context "request to clear screen:" do
				cli, game_selector_view = create_game_selector_view
				game_selector_view.clear_screen

				it "asks the CLI to clear the screen" do
					cli.clear_screen_called.should be_true
				end
			end

			context "display options:" do
				cli, game_selector_view = create_game_selector_view
				game_selector_view.list_game_options(GameSelector.game_options)

				it "writes formatted game options to CLI" do
					cli.write_received.should eq("<1> Human vs Human\n<2> Human vs Computer\n<3> Computer vs Human\n<4> Computer vs Computer")
				end
			end

			context "get user input:" do
				cli, game_selector_view = create_game_selector_view
				user_input = "user_input"
				cli.read_responds_with = user_input

				it "responds with user's input" do
					game_selector_view.request_selection.should eq(user_input)
				end
			end

			context "alert invaid input:" do
				cli, game_selector_view = create_game_selector_view
				game_selector_view.report_invalid_input

				it "writes error to client" do
					cli.write_received.should eq(INVALID_USER_INPUT_MESSAGE)
				end
			end
		end

		def self.create_game_selector_view
			cli = CLIDouble.new
			game_selector_view = GameSelectorView.new(cli)
			{cli, game_selector_view}
		end
	end
end
