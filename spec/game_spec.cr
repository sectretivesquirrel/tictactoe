require "./spec_helper"
require "../src/game/game"
require "../src/game/board"
require "./double/board_double"

module TicTacToe
	module Game
		describe Game do
			it "makes moves" do
				game = TicTacToe.create_game
				game.move!(1)
				game.board.squares.should eq([X, 2, 3, 4, 5, 6, 7, 8, 9])
			end

			context "a cloned board" do
				cloaning_game = TicTacToe.create_game
				cloaning_game.move!(2)
				frozen_board = cloaning_game.board.clone
				cloaned_game = cloaning_game.clone
				cloaning_game.move!(1)

				it "has its own board" do
					cloaned_game.board.should_not be(cloaning_game.board)
				end

				it "has the same board squares as the cloning game board had at the time it was cloned" do
					cloaned_game.board.squares.should eq(frozen_board.squares)
				end
			end

			context "with a new board" do
				game = TicTacToe.create_game

				it "is not over" do
					game.over?.should be_false
				end

				it "is not drawn" do
					game.draw?.should be_false
				end
			end

			context "drawn game" do
				board = BoardDouble.new(DRAWN_BOARD_SQUARES)
				game = TicTacToe.create_game(board)

				it "is over" do
					game.over?.should be_true
				end

				it "is drawn" do
					game.draw?.should be_true
				end
			end

			context "with a winning board for X" do
				board = BoardDouble.new(X_WINNING_BOARD_SQUARES)
				game = TicTacToe.create_game(board)

				it "is over" do
					game.over?.should be_true
				end

				it "is not drawn" do
					game.draw?.should be_false
				end

				it "reports that winner is X" do
					game.winner.should eq(X)
				end
			end

			context "with a winning board for O" do
				board = BoardDouble.new(O_WINNING_BOARD_SQUARES)
				game = TicTacToe.create_game(board)

				it "is over" do
					game.over?.should be_true
				end

				it "is not drawn" do
					game.draw?.should be_false
				end

				it "reports that winner is O" do
					game.winner.should eq(O)
				end
			end

			context "with an incomplete game" do
				game = TicTacToe.create_game
				game.move!(3)
				game.move!(4)

				it "reports that all squares are available except those that have been taken" do
					game.available_squares.should eq([1, 2, 5, 6, 7, 8, 9])
				end
			end

		end
	end
end
