require "./spec_helper"
require "../src/app/game_view"
require "./double/cli_double"
require "./double/board_double"

module TicTacToe
	module App
		describe GameView do
			context "request to clear screen:" do
				game_view, cli = create_game_view
				game_view.clear_screen

				it "asks the CLI to clear the screen" do
					cli.clear_screen_called.should be_true
				end
			end

			context "request to display a board:" do
				[X_WINNING_BOARD_SQUARES, O_WINNING_BOARD_SQUARES, DRAWN_BOARD_SQUARES].each do |squares|
					it "asks the CLI to write the formatted board" do
						game_view, cli = create_game_view
						board = Game::BoardDouble.new(squares)
						game_view.display_board(board)
						cli.write_received.should eq(BoardFormatter.new.format(board))
					end
				end
			end

			context "request current player's move:" do
				[X, O].each do |player|
					user_input = "user input"
					game = TicTacToe.create_game
					game_view, cli = create_game_view
					cli.read_responds_with = user_input
					result = game_view.request_move(player)

					it "asks the CLI to write 'Player #{player}'s move:' to CLI" do
						cli.write_received.should match(/Player #{player}'s move:/)
					end

					it "responds with input read from the CLI" do
						result.should eq(user_input)
					end
				end
			end

			context "report game's status:" do
				it "with a new game, sends the message 'Game in progress...' to the CLI" do
					game_view, cli = create_game_view
					game_view.report_game_status(TicTacToe.create_game)
					cli.write_received.should match(/Game in progress\.\.\./)
				end

				it "with a game won by X, asks the CLI to write 'X Wins!'" do
					game_view, cli = create_game_view
					game = TicTacToe.create_game(Game::BoardDouble.new(X_WINNING_BOARD_SQUARES))
					game_view.report_game_status(game)
					cli.write_received.should match(/X Wins!/)
				end

				it "with a game won by O, asks the CLI to write 'O Wins!'" do
					game_view, cli = create_game_view
					game = TicTacToe.create_game(Game::BoardDouble.new(O_WINNING_BOARD_SQUARES))
					game_view.report_game_status(game)
					cli.write_received.should match(/O Wins!/)
				end

				it "with a drawn game, asks the CLI to write 'It's a Draw!'" do
					game_view, cli = create_game_view
					game = TicTacToe.create_game(Game::BoardDouble.new(DRAWN_BOARD_SQUARES))
					game_view.report_game_status(game)
					cli.write_received.should match(/It's a Draw!/)
				end
			end
		end

		context "warn invalid input" do
			it "asks the cli to write an invalid input message" do
				game_view, cli = create_game_view
				game_view.warn_invalid_input
				cli.write_received.should eq(INVALID_INPUT_MESSAGE)
			end
		end

		def self.create_game_view
			cli = CLIDouble.new
			game_view = GameView.new(cli)
			{game_view, cli}
		end
	end
end
