require "./spec_helper"
require "../src/app/move_validator"
require "../src/game/game"

module TicTacToe
	module App
		describe MoveValidator do
			context "user inputs valid move" do
				valid_move = "2"
				validator = MoveValidator.new(TicTacToe.create_game)

				it "is valid" do
					validator.valid?(valid_move).should be_true
				end

				it "ignores whitespace" do
					validator.valid?(" #{valid_move} ").should be_true
				end

				it "parses to an integer" do
					validator.parse(valid_move).should eq(2)
				end
			end

			context "nil input" do
				validator = MoveValidator.new(TicTacToe.create_game)
				run_invalid_tests(validator, nil)
			end

			context "non-integer input" do
				validator = MoveValidator.new(TicTacToe.create_game)
				run_invalid_tests(validator, "blah")
			end

			context "out of bounds integer input" do
				validator = MoveValidator.new(TicTacToe.create_game)
				run_invalid_tests(validator, "10")
			end

			context "square taken input" do
				game = TicTacToe.create_game
				game.move!(5)
				validator = MoveValidator.new(game)
				run_invalid_tests(validator, "5")
			end
		end

		def self.run_invalid_tests(validator : MoveValidator, invalid_input : String?)
			it "is not valid" do
				validator.valid?(invalid_input).should be_false
			end

			it "throws error if parsed" do
				expect_raises(MoveValidatorException, /Invalid move - please check move is valid before calling parse/) do
					validator.parse(invalid_input)
				end
			end
		end
	end
end
