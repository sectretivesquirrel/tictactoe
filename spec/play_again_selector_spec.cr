require "./spec_helper"
require "../src/app/play_again_selector"
require "./double/cli_double"

module TicTacToe
	module App
		describe PlayAgainSelector do
			cli = CLIDouble.new
			play_again_selector = PlayAgainSelector.new(cli)

			it "ask the CLI to write 'Type <Yy> to play again'" do
				play_again_selector.play_again?
				cli.write_received.should eq("Type <Yy> to play again")
			end

			context "User Inputs <Y>" do
				it "responds with true" do
					cli.read_responds_with = "Y"
					play_again_selector.play_again?.should be_true
				end
			end

			context "User Inputs < Y >" do
				it "ignores white space and responds with true" do
					cli.read_responds_with = " Y "
					play_again_selector.play_again?.should be_true
				end
			end

			context "User Inputs < y >" do
				it "ignores case and responds with true" do
					cli.read_responds_with = " y "
					play_again_selector.play_again?.should be_true
				end
			end

			context "User Inputs <Yy>" do
				it "multiple ys are fine, responds true" do
					cli.read_responds_with = "Yy"
					play_again_selector.play_again?.should be_true
				end
			end

			context "User Inputs <n>" do
				it "returns false" do
					cli.read_responds_with = "n"
					play_again_selector.play_again?.should be_false
				end
			end

			context "User Inputs <yn>" do
				it "returns false" do
					cli.read_responds_with = "yn"
					play_again_selector.play_again?.should be_false
				end
			end

			context "User Inputs <ny>" do
				it "returns false" do
					cli.read_responds_with = "ny"
					play_again_selector.play_again?.should be_false
				end
			end
		end
	end
end
