require "./spec_helper"
require "../src/player/player_factory"
require "../src/player/player_type"
require "../src/player/human_player"
require "../src/player/computer_player"

module TicTacToe
	module Player
		describe PlayerFactory do
			context "with a human player type" do
				player_factory = PlayerFactory.new

				it "builds a human player" do
					player_factory.build(PlayerType::Human).should be_a(HumanPlayer)
				end
			end

			context "with a computer player type" do
				player_factory = PlayerFactory.new

				it "builds a computer player" do
					player_factory.build(PlayerType::Computer).should be_a(ComputerPlayer)
				end
			end
		end
	end
end
