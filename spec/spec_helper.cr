require "spec"
require "../src/app/colour_formatter"
require "../src/game/game"
require "../src/game/player_symbol"
require "../src/game/board"
require "../src/player/player"
require "./double/player_double"
require "../src/ai/mini_max"
require "../src/ai/concurrent_mini_max"
require "../src/ai/alpha_beta"
require "../src/ai/one_deep_concurrent_mini_max"

module TicTacToe
	X = Game::PlayerSymbol::X
	O = Game::PlayerSymbol::O

	DRAWN_BOARD_SQUARES =
		[X, O, X,
		 O, X, X,
		 O, X, O] of Game::BoardValue

	X_WINNING_BOARD_SQUARES =
		[X, X, X,
	 	 O, O, 6,
		 7, 8, 9]

	O_WINNING_BOARD_SQUARES =
		[X, O, X,
	   O, O, X,
		 X, O, 9]

	WINNING_ROW_SQUARES =
		[X, X, X,
	   O, O, 6,
	   7, 8, 9]

	WINNING_COLUMN_SQUARES =
		[X, O, X,
	   O, O, X,
		 X, O, 9]

	WINNING_DIAGANOL_SQUARES =
		[O, 2, 3,
	   4, O, X,
		 X, X, O]

	def self.create_game(board = Game::Board.new) : Game::Game
		x = Player::PlayerDouble.new
		o = Player::PlayerDouble.new
		Game::Game.new(board, x, o)
	end

	def self.create_board(squares : Game::Line) : Game::BoardDouble
		Game::BoardDouble.new(squares)
	end

	def self.make_moves(board : Game::Board, moves : Game::Squares)
		moves.each { |square| board.move!(square) }
	end

	def self.formatted_board_string(formatted_squares : Array(String | Int32)) : String
		a, b, c, d, e, f, g, h, i = formatted_squares
		" #{a} | #{b} | #{c} \n--- --- ---\n #{d} | #{e} | #{f} \n--- --- ---\n #{g} | #{h} | #{i} "
	end

	def self.create_game_with_moves(moves : Game::Squares) : Game::Game
		game = create_game
		moves.each { |square| game.move!(square) }
		game
	end

	def self.run_ai_tests(game : Game::Game, move : Game::Square, max_depth : Int32? = nil)
		(max_depth.nil? ? AI::MiniMax.new : AI::MiniMax.new(max_depth: max_depth)).select_move(game).first.should eq(move)
		(max_depth.nil? ? AI::AlphaBeta.new : AI::AlphaBeta.new(max_depth: max_depth)).select_move(game).first.should eq(move)
		(max_depth.nil? ? AI::ConcurrentMiniMax.new : AI::ConcurrentMiniMax.new(max_depth: max_depth)).select_move(game).first.should eq(move)
		(max_depth.nil? ? AI::OneDeepConcurrentMiniMax.new : AI::OneDeepConcurrentMiniMax.new(max_depth: max_depth)).select_move(game).first.should eq(move)
	end
end
