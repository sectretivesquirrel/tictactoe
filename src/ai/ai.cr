require "./playable"

module TicTacToe
	module AI
		module AI
			abstract def select_move(game : Playable) : PlayResult
		end
	end
end
