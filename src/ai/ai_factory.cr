require "./ai_type"
require "./mini_max"
require "./concurrent_mini_max"
require "./alpha_beta"

module TicTacToe
	module AI
		class AIFactory
			DEFAULT_MAX_DEPTH = 9

			def build(ai_type : AIType, max_depth = DEFAULT_MAX_DEPTH) : AI
				ai(ai_type).new(max_depth: max_depth)
			end

			private def ai(ai_type : AIType)
				case ai_type
				when AIType::MiniMax
					MiniMax
				when AIType::ConcurrentMiniMax
					ConcurrentMiniMax
				when AIType::AlphaBeta
					AlphaBeta
				when AIType::OneDeepConcurrentMiniMax
					OneDeepConcurrentMiniMax
				else
					raise AIFactoryException.unsupported_ai
				end
			end
		end

		class AIFactoryException < Exception
			UNSUPPORTED_AI_MESSAGE = "Unsupported AI Type"

			def self.unsupported_ai : AIFactoryException
				self.new(UNSUPPORTED_AI_MESSAGE)
			end
		end
	end
end
