module TicTacToe
	module AI
		enum AIType
			MiniMax, ConcurrentMiniMax, AlphaBeta, OneDeepConcurrentMiniMax
		end
	end
end
