require "./mini_max"

module TicTacToe
	module AI
		alias AlphaBetaInitialiser = NamedTuple(maximising: Bool, depth: Int32, max_depth: Int32, alpha: PlayScore, beta: PlayScore)

		class AlphaBeta < MiniMax

			DEFAULT_ALPHA = -100
			DEFAULT_BETA = 100
			DEFAULAT_MAX_DEPTH = 100

			@best_result : PlayResult

			def initialize(@maximising = true, @depth = 0, @max_depth = DEFAULAT_MAX_DEPTH, @alpha = DEFAULT_ALPHA, @beta = DEFAULT_BETA)
				@best_result = dead_path_result
			end

			def select_move(base_game : Playable) : PlayResult
				run_moves!(base_game)
				best_result
			end

			private def dead_path_result : PlayResult
				{-1, best_score}
			end

			private def best_score: PlayScore
				if @maximising
					@alpha
				else
					@beta
				end
			end

			private def run_moves!(base_game : Playable)
				possible_moves(base_game).each do |square|
					current_result = move_result(base_game, square)
					update_result!(current_result) if better_result?(current_result)
					break if dead_end?
				end
			end

			private def better_result?(current_result : PlayResult) : Bool
				_, current_score = current_result
				@maximising ? (current_score > @alpha) : (current_score < @beta)
			end

			private def update_result!(better_result : PlayResult)
				@best_result = better_result
				update_alpha_beta!(better_result)
			end

			private def update_alpha_beta!(better_result : PlayResult)
				_, better_score = better_result
				if @maximising
					@alpha = better_score
				else
					@beta = better_score
				end
			end

			private def dead_end? : Bool
				@beta <= @alpha
			end

			private def best_result : PlayResult
				@best_result
			end

			private def recursive_ai : AI
				self.class.new(**alpha_beta_recursive_args)
			end

			private def alpha_beta_recursive_args : AlphaBetaInitialiser
				recursive_args.merge({alpha: @alpha, beta: @beta})
			end
		end
	end
end
