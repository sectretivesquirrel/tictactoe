require "./mini_max"

module TicTacToe
	module AI
		alias PlayResultTransport = {PlayResult, Int32}
		alias PlayResultChannel = Channel(PlayResultTransport)

		class ConcurrentMiniMax < MiniMax
			private def move_results(base_game : Playable) : Array(PlayResult)
				channel = build_channel
				spawn_moves(channel, base_game)
				get_results(channel, base_game)
			end

			private def build_channel : PlayResultChannel
				Channel(PlayResultTransport).new
			end

			private def spawn_moves(channel : PlayResultChannel, base_game : Playable)
				possible_moves(base_game).each_with_index do	|square, index|
					spawn(run_move(channel, base_game, square, index))
				end
			end

			private def run_move(channel : PlayResultChannel, base_game : Playable, square : Play, index : Int32)
				channel.send({move_result(base_game, square), index})
			end

			private def get_results(channel : PlayResultChannel, base_game : Playable) : Array(PlayResult)
				results = [] of PlayResultTransport

				until all_results_received?(base_game, results)
					results << channel.receive
				end

				sync_results!(results)
			end

			private def all_results_received?(base_game : Playable, results : Array(PlayResultTransport)) : Bool
				results.size == possible_moves(base_game).size
			end

			private def sync_results!(results : Array(PlayResultTransport)) : Array(PlayResult)
				results.sort_by! { |_, index| index }
				results.map { |move_result, _| move_result }
			end
		end
	end
end
