require "./ai"
require "./playable"

module TicTacToe
	module AI
		alias MiniMaxInitialiser = NamedTuple(maximising: Bool, depth: Int32, max_depth: Int32)

		class MiniMax
			include AI

			@@win_score = 100
			@@draw_score = 0

			def initialize(@maximising = true, @depth = 0, @max_depth = 9)
			end

			def select_move(base_game : Playable) : PlayResult
				if @maximising
					maximising_move(base_game)
				else
					minimising_move(base_game)
				end
			end

			private def maximising_move(base_game : Playable) : PlayResult
				move_results(base_game).max_by(&.last)
			end

			private def minimising_move(base_game : Playable) : PlayResult
				move_results(base_game).min_by(&.last)
			end

			private def move_results(base_game : Playable) : Array(PlayResult)
				possible_moves(base_game).map { |square| move_result(base_game, square).as(PlayResult) }
			end

			private def possible_moves(base_game : Playable) : Plays
				base_game.available_squares
			end

			private def move_result(base_game : Playable, square : Play) : PlayResult
				played_game = play_move(base_game, square)
				{square, game_score(played_game)}
			end

			private def play_move(base_game, square : Play) : Playable
				cloned_game = base_game.clone
				cloned_game.move!(square)
				cloned_game
			end

			private def game_score(played_game : Playable) : PlayScore
				if played_game.winner?
					win_score
				elsif played_game.draw? || at_max_depth
					@@draw_score
				else
					recursive_score(played_game)
				end
			end

			private def win_score : PlayScore
				if @maximising
					@@win_score - @depth
				else
					-@@win_score + @depth
				end
			end

			private def at_max_depth : Bool
				next_depth == @max_depth
			end

			private def next_depth : Int32
				@depth + 1
			end

			private def recursive_score(played_game : Playable) : PlayScore
				recursive_ai.select_move(played_game).last
			end

			private def recursive_ai : AI
				self.class.new(**recursive_args)
			end

			private def recursive_args : MiniMaxInitialiser
				{maximising: !@maximising, depth: next_depth, max_depth: @max_depth}
			end
		end
	end
end
