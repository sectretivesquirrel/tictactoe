require "./concurrent_mini_max"

module TicTacToe
	module AI
		class OneDeepConcurrentMiniMax < ConcurrentMiniMax
			private def recursive_ai : AI
				MiniMax.new(**recursive_args)
			end
		end
	end
end
