module TicTacToe
	module AI
		alias PlayScore = Int32
		alias Play = Int32
		alias Plays = Array(Play)
		alias PlayResult = Tuple(Play, PlayScore)

		module Playable
			abstract def clone : Playable
			abstract def move!(play : Play)
			abstract def available_squares : Plays
			abstract def winner? : Bool
			abstract def draw? : Bool
		end
	end
end
