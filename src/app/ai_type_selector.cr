require "../ai/ai_type"

module TicTacToe
	module App
		class AITypeSelector

			@@ai_types = {
				AI::AIType::AlphaBeta,
				AI::AIType::MiniMax,
				AI::AIType::ConcurrentMiniMax,
				AI::AIType::OneDeepConcurrentMiniMax
			}

			def initialize(@cli : CLI)
			end

			def get_selection : AI::AIType
				@cli.clear_screen
				@cli.write(ai_type_options)
				@@ai_types[get_input]
			end

			private def ai_type_options
				@@ai_types.map_with_index do |ai_type, index|
					format_ai_type_option(ai_type, index + 1)
				end.join("\n")
			end

			private def format_ai_type_option(ai_type : AI::AIType, number : Int32) : String
				formatted_option = "<#{number}> #{ai_type}"
				if ai_type.alpha_beta?
					"#{formatted_option} (* default)"
				else
					formatted_option
				end
			end

			private def get_input : Int32
				input = @cli.read
				if input.nil? || invalid_input?(input)
					0
				else
					input.to_i - 1
				end
			end

			private def invalid_input?(input : String) : Bool
				/^\s*[1234]\s*$/.match(input).nil?
			end
		end
	end
end
