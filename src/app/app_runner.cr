require "./game_selector"
require "./game_view"
require "./game_controller"
require "./game_runner"
require "./play_again_selector"
require "./ai_type_selector"
require "../game/game_factory"

module TicTacToe
	module App
		class AppRunner
			def initialize(
				@game_selector : GameSelector,
				@game_factory : Game::GameFactory,
				@game_view : GameView,
				@play_again_selector : PlayAgainSelector,
				@ai_type_selector : AITypeSelector)
			end

			def run
				game_runner.run
				run if @play_again_selector.play_again?
			end

			private def game_runner : GameRunner
				game_controller = GameController.new(game, @game_view)
				GameRunner.new(game_controller)
			end

			private def game : Game::Game
				game_type = @game_selector.get_selection
				if involves_ai?(game_type)
					ai_type = @ai_type_selector.get_selection
				end
				@game_factory.build(game_type, ai_type)
			end

			private def involves_ai?(game_type : GameType) : Bool
				game_type.any? &.computer?
			end
		end
	end
end
