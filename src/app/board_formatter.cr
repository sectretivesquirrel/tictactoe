require "../game/board"
require "../game/board_status"
require "./colour_formatter"

module TicTacToe
	module App
		class BoardFormatter
			@@row_divider = "--- --- ---"

			def initialize
				@colour_formatter = ColourFormatter.new
			end

			def format(board : Game::Board) : String
				row1, row2, row3 = format_rows(board)
				[row1, @@row_divider, row2, @@row_divider, row3].join("\n")
			end

			private def format_rows(board : Game::Board) : Array(String)
				all_rows(board).map_with_index { |row, row_index| format_row(row, row_index, board) }
			end

			private def all_rows(board : Game::Board) : Game::Lines
				Game::BoardStatus.new(board).rows
			end

			private def format_row(row : Game::Line, row_index : Int32, board : Game::Board) : String
				(row.map_with_index do |square_value, square_index|
					" #{format_square(square_value, square_number(row_index, square_index, board) , board)} "
				end).join("|")
			end

			private def square_number(row_index : Int32, square_index : Int32, board : Game::Board) : Game::Square
				row_index * board.size + square_index + 1
			end

			private def format_square(square_value : Game::BoardValue, square_number : Game::Square, board : Game::Board) : String
				if square_value.is_a?(Game::PlayerSymbol)
					format_player_symbol(square_value, square_number, board)
				else
					square_value.to_s
				end
			end

			private def format_player_symbol(player_symbol : Game::PlayerSymbol, square_number : Game::Square, board : Game::Board) : String
				if board.last_move == square_number
					@colour_formatter.red(player_symbol)
				elsif player_symbol.x?
					@colour_formatter.yellow(player_symbol)
				else
					@colour_formatter.magenta(player_symbol)
				end
			end
		end
	end
end
