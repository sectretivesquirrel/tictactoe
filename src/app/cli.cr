module TicTacToe
	module App
		class CLI
			def write(input : String) : Nil
				::puts(input)
			end

			def read() : String?
				::gets()
			end

			def clear_screen
				print "\e[H\e[2J"
			end
		end
	end
end
