module TicTacToe
	module App
		class ColourFormatter
			macro define_colours(*colours)
				{% for colour in colours %}
					def {{colour[:name]}}(content) : String
						"\u001B[#{{{colour[:code]}}}m#{content}\u001B[0m"
					end
				{% end %}
			end

			define_colours(
				{name: red, code: 31},
				{name: green, code: 32},
				{name: yellow, code: 33},
				{name: blue, code: 34},
				{name: magenta, code: 35}
			)
		end
	end
end
