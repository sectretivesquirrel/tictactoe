require "./move_validator"

module TicTacToe
	module App
		class GameController
			def initialize(@game : Game::Game, @game_view : GameView)
				@validator = MoveValidator.new(@game)
			end

			def next? : Bool
				next_game_state? = !@game.over?
				run_game_state
				next_game_state?
			end

			private def run_game_state
				clear_screen
				display_board
				report_result_or_run_next_move
			end

			private def clear_screen
				@game_view.clear_screen
			end

			private def display_board
				@game_view.display_board(@game.board)
			end

			private def report_result_or_run_next_move
				if @game.over?
					report_result
				else
					run_move
				end
			end

			private def report_result
				@game_view.report_game_status(@game)
			end

			private def run_move
				@game.move!(current_move)
			end

			private def current_move : Game::Square
				if current_player.human?
					human_move
				else
					computer_player_move
				end
			end

			private def current_player : Player::Player
				@game.current_player
			end

			private def human_move : Game::Square
				parse_or_reject_move(unparsed_human_move)
			end

			private def unparsed_human_move : String?
				@game_view.request_move(@game.current_player_symbol)
			end

			private def parse_or_reject_move(raw_move : String?) : Game::Square
				if @validator.valid?(raw_move)
					@validator.parse(raw_move)
				else
					@game_view.warn_invalid_input
					human_move
				end
			end

			private def computer_player_move : Game::Square
				current_player.as(Player::ComputerPlayer).select_move(@game)
			end
		end
	end
end
