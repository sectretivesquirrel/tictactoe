require "./game_controller"

module TicTacToe
	module App
		class GameRunner
			def initialize(@game_controller : GameController)
			end

			def run
				run if @game_controller.next?
			end
		end
	end
end
