require "./cli"
require "./game_selector_view"
require "../player/player_type"

module TicTacToe
	module App
		class GameSelector
			@@game_options = [
				{Player::PlayerType::Human, Player::PlayerType::Human},
				{Player::PlayerType::Human, Player::PlayerType::Computer},
				{Player::PlayerType::Computer, Player::PlayerType::Human},
				{Player::PlayerType::Computer, Player::PlayerType::Computer}]

			def initialize(@view : GameSelectorView)
			end

			def self.game_options
				@@game_options
			end

			def get_selection : GameType
				clear_screen
				list_game_options
				get_valid_user_selection
			end

			private def clear_screen
				@view.clear_screen
			end

			private def list_game_options
				@view.list_game_options(@@game_options)
			end

			private def get_valid_user_selection : GameType
				input = request_selection

				if input.nil? || invalid_input?(input)
					report_invalid_input
					get_valid_user_selection
				else
					game_option(input.to_i)
				end
			end

			private def request_selection : String?
				@view.request_selection
			end

			private def invalid_input?(input : String) : Bool
				numeric_input?(input) || input_out_of_range?(input)
			end

			private def numeric_input?(input : String): Bool
				/\d+/.match(input).nil?
			end

			private def input_out_of_range?(input : String) : Bool
				!(1..total_game_options).includes?(input.to_i)
			end

			private def total_game_options : Int32
				@@game_options.size
			end

			private def report_invalid_input
				@view.report_invalid_input
			end

			private def game_option(game_number : Int32) : GameType
				@@game_options[game_number - 1]
			end
		end
	end
end
