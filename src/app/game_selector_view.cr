require "./cli"
require "../player/player_type"

module TicTacToe
	module App
		INVALID_USER_INPUT_MESSAGE = "Oops, please enter a valid game option"

		class GameSelectorView
			def initialize(@cli : CLI)
			end

			def clear_screen
				@cli.clear_screen
			end

			def list_game_options(game_options : Array(GameType))
				@cli.write(game_options_list(game_options))
			end

			def request_selection
				@cli.read
			end

			def report_invalid_input
				@cli.write(INVALID_USER_INPUT_MESSAGE)
			end

			private def game_options_list(game_options : Array(GameType)) : String
				formatted_game_types(game_options).join("\n")
			end

			private def formatted_game_types(game_options : Array(GameType)) : Array(String)
				game_options.map_with_index do |(player_x, player_o), index|
					"<#{index + 1}> #{player_x} vs #{player_o}"
				end
			end
		end
	end
end
