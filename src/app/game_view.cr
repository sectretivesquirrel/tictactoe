require "../game/game"
require "../game/board"
require "../game/player_symbol"
require "./cli"
require "./board_formatter"

module TicTacToe
	module App
		INVALID_INPUT_MESSAGE = "Oops! That's not right. Please try again"

		class GameView
			def initialize(@cli : CLI, @board_formatter = BoardFormatter.new)
			end

			def report_game_status(game : Game::Game)
				if game.over?
					report_completed_game(game)
				else
					report_active_game
				end
			end

			def display_board(board : Game::Board)
				@cli.write(@board_formatter.format(board))
			end

			def request_move(player_symbol : Game::PlayerSymbol) : String?
				@cli.write("Player #{player_symbol}'s move:")
				@cli.read
			end

			def clear_screen
				@cli.clear_screen
			end

			def warn_invalid_input
				@cli.write(INVALID_INPUT_MESSAGE)
			end

			private def report_active_game
				@cli.write("Game in progress...")
			end

			private def report_completed_game(game : Game::Game)
				if game.draw?
					@cli.write("It's a Draw!")
				else
					@cli.write("#{game.winner} Wins!")
				end
			end
		end
	end
end
