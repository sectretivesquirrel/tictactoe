module TicTacToe
	module App
		class MoveValidator
			def initialize(@game : Game::Game)
			end

			def valid?(human_move : String?) : Bool
				!human_move.nil? && valid_string?(human_move)
			end

			def parse(human_move : String?) : Int32
				if !human_move.nil? && valid_string?(human_move)
					human_move.to_i
				else
					raise MoveValidatorException.new("Invalid move - please check move is valid before calling parse")
				end
			end

			private def valid_string?(human_move : String) : Bool
				valid_integer?(human_move) && square_available?(human_move.to_i)
			end

			private def valid_integer?(human_move : String) : Bool
				!(/^\d+$/.match(human_move.strip).nil?)
			end

			private def square_available?(square : Int32) : Bool
				@game.square_available?(square)
			end
		end

		class MoveValidatorException < Exception
		end
	end
end
