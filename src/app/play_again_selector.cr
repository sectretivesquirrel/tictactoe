require "./cli"

module TicTacToe
	module App
		class PlayAgainSelector
			def initialize(@cli : CLI)
			end

			def play_again? : Bool
				ask_to_play_again
				get_play_again
			end

			private def ask_to_play_again
				@cli.write("Type <Yy> to play again")
			end

			private def get_play_again
				!(@cli.read =~ /^\s*y+\s*$/i).nil?
			end
		end
	end
end
