require "./player_symbol"

module TicTacToe
	module Game
		alias Square = Int32
		alias BoardValue = Square | PlayerSymbol
		alias Squares = Array(Square)
		alias Line = Array(BoardValue)

		class Board
			getter :squares, :moves

			@@total_squares = 9

			@squares : Line

			def initialize(@squares = empty_squares, @moves = [] of Square)
			end

			def clone : Board
				Board.new(@squares.dup, @moves.dup)
			end

			def size : Int32
				Math.sqrt(@@total_squares).to_i
			end

			def move!(square : Square)
				if square_available?(square)
					make_move!(square)
				else
					raise BoardException.illegal_move
				end
			end

			def last_move : Square?
				@moves.last?
			end

			def current_player_symbol : PlayerSymbol
				if @moves.size.even?
					PlayerSymbol::X
				else
					PlayerSymbol::O
				end
			end

			def available_squares : Squares
				all_squares.reject { |square| @moves.includes?(square) }
			end

			def square_available?(square : Square) : Bool
				available_squares.includes?(square)
			end

			private def empty_squares : Array(BoardValue)
				all_squares.map(&.as(BoardValue))
			end

			private def make_move!(square : Square)
				set_square!(square)
				update_moves!(square)
			end

			private def update_moves!(square : Square)
				@moves << square
			end

			private def set_square!(square : Square)
				@squares[square - 1] = current_player_symbol
			end

			private def all_squares : Squares
				(1..@@total_squares).to_a
			end
		end

		class BoardException < Exception
			ILLEGAL_MOVE_EXCEPTION = "Illegal move: requested square already taken"

			def self.illegal_move : BoardException
				self.new(ILLEGAL_MOVE_EXCEPTION)
			end
		end
	end
end
