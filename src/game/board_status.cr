require "./board"

module TicTacToe
	module Game
		alias Lines = Array(Line)
		alias Indices = Array(Array(Int32))

		class BoardStatus
			@@row_indices = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
			@@column_indices = [[0, 3, 6], [1, 4, 7], [2, 5, 8]]
			@@diagonal_indices = [[0, 4, 8], [2, 4, 6]]

			def initialize(@board : Board)
			end

			def rows : Lines
				map_indices_to_values(@@row_indices)
			end

			def complete? : Bool
				full? || winner?
			end

			def winner? : Bool
				winning_row? || winning_column? || winning_diaganol?
			end

			def winner : PlayerSymbol?
				winning_line = all_lines.find { |line| line_wins?(line) }
				to_winner(winning_line)
			end

			def drawn? : Bool
				full? && !winner?
			end

			def full? : Bool
				squares.all?(&.is_a?(PlayerSymbol))
			end

			private def map_indices_to_values(indices : Indices) : Lines
				indices.map(&.map { |i| squares[i] })
			end

			private def squares : Line
				@board.squares
			end

			private def columns : Lines
				map_indices_to_values(@@column_indices)
			end

			private def diagonals : Lines
				map_indices_to_values(@@diagonal_indices)
			end

			private def winning_row? : Bool
				winning_line?(rows)
			end

			private def winning_column? : Bool
				winning_line?(columns)
			end

			private def winning_diaganol? : Bool
				winning_line?(diagonals)
			end

			private def winning_line?(lines : Lines) : Bool
				lines.any? { |line| line.uniq.size == 1 }
			end

			private def all_lines : Lines
				[rows, columns, diagonals].flat_map { |x| x }
			end

			private def line_wins?(line : Line) : Bool
				line.uniq.size == 1
			end

			private def to_winner(winning_line : Line?) : PlayerSymbol?
				winning_line.nil? ? nil : winning_line.first.as?(PlayerSymbol)
			end
		end
	end
end
