require "./board"
require "./board_status"
require "../player/player"
require "../ai/playable"

module TicTacToe
	module Game
		class Game
			include AI::Playable

			getter :board

			def initialize(@board : Board, @player_x : Player::Player, @player_o : Player::Player)
				@board_status = BoardStatus.new(@board)
			end

			def clone : Game
				Game.new(@board.clone, @player_x, @player_o)
			end

			def over? : Bool
				@board_status.complete?
			end

			def draw? : Bool
				over? && !@board_status.winner?
			end

			def winner? : Bool
				@board_status.winner?
			end

			def winner : PlayerSymbol?
				@board_status.winner
			end

			def current_player_symbol : PlayerSymbol
				@board.current_player_symbol
			end

			def move!(square : Square)
				@board.move!(square)
			end

			def available_squares : Array(Int32)
				@board.available_squares
			end

			def current_player : Player::Player
				if current_player_symbol.x?
					@player_x
				else
					@player_o
				end
			end

			def square_available?(square : Int32) : Bool
				@board.square_available?(square)
			end
		end
	end
end
