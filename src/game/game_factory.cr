require "./game"
require "../player/player_factory"
require "../player/player_type"
require "../ai/ai_type"

module TicTacToe
	alias GameType = Tuple(Player::PlayerType, Player::PlayerType)

	module Game
		class GameFactory
			def initialize
				@player_factory = Player::PlayerFactory.new
			end

			def build(game_type : GameType, ai_type : AI::AIType?) : Game
				Game.new(Board.new, **players(game_type, ai_type))
			end

			private def players(game_type : GameType, ai_type : AI::AIType?) : NamedTuple(player_x: Player::Player, player_o: Player::Player)
				x, o = game_type
				{player_x: build_player(x, ai_type), player_o: build_player(o, ai_type)}
			end

			private def build_player(player_type : Player::PlayerType, ai_type : AI::AIType?) : Player::Player
				@player_factory.build(player_type, ai_type)
			end
		end
	end
end
