require "./player"
require "../ai/*"

module TicTacToe
	module Player
		class ComputerPlayer < Player

			@ai_type : AI::AIType
			@sleep_time : Int32 | Float64

			def initialize(@ai_factory : AI::AIFactory, ai_type : AI::AIType? = nil, @sleep_time = 0.5)
				if ai_type.nil?
					@ai_type = AI::AIType::AlphaBeta
				else
					@ai_type = ai_type
				end
			end

			def human?
				false
			end

			def select_move(playable : AI::Playable) : AI::PlayScore
				sleep(@sleep_time.seconds) unless @sleep_time == 0
				ai.select_move(playable).first
			end

			private def ai : AI::AI
				@ai_factory.build(@ai_type, max_depth: 6)
			end
		end
	end
end
