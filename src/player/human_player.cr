require "./player"

module TicTacToe
	module Player
		class HumanPlayer < Player
			def human?
				true
			end
		end
	end
end
