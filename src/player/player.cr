require "./player_type"

module TicTacToe
	module Player
		abstract class Player
			abstract def human? : Bool
		end
	end
end
