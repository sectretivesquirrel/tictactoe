require "./player_type"
require "./human_player"
require "./computer_player"
require "../ai/ai_factory"

module TicTacToe
	module Player
		class PlayerFactory
			def initialize
				@ai_factory = AI::AIFactory.new
			end

			def build(player_type : PlayerType, ai_type : AI::AIType? = nil) : Player
				if player_type.human?
					HumanPlayer.new
				else
					ComputerPlayer.new(@ai_factory, ai_type)
				end
			end
		end
	end
end
