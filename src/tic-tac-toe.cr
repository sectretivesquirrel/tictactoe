require "./tic-tac-toe/*"
require "./app/cli"
require "./app/app_runner"
require "./app/game_selector"
require "./app/game_selector_view"
require "./app/game_view"
require "./app/play_again_selector"
require "./app/ai_type_selector"
require "./game/game_factory"

module TicTacToe
	module App
		cli = CLI.new
		game_selector_view = GameSelectorView.new(cli)
		game_selector = GameSelector.new(game_selector_view)
		game_factory = Game::GameFactory.new
		game_view = GameView.new(CLI.new)
		play_again_selector = PlayAgainSelector.new(cli)
		ai_type_selector = AITypeSelector.new(cli)
		app_runner = AppRunner.new(game_selector, game_factory, game_view, play_again_selector, ai_type_selector)
		app_runner.run
	end
end

